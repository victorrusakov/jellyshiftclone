﻿using Game.Data;
using UnityEngine;

namespace Advertisement
{
    public class ADSManager : MonoBehaviour
    {
        void OnEnable()
        {
            IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;

            if (PlayerPrefs.GetInt("Ads") != 1)
            {
                IronSource.Agent.init("e80bd329");
                IronSource.Agent.loadInterstitial();
                IronSource.Agent.validateIntegration();
                DontDestroyOnLoad(this);
            }
        }

        void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
        {
            Debug.Log(20);
            DataController.Instance().Data.Coins += placement.getRewardAmount();
            DataController.Instance().OnBuy();
            DataController.Instance().LocalSave();
        }
        void InterstitialAdReadyEvent()
        {
            IronSource.Agent.isInterstitialReady();
        }
    }
}
