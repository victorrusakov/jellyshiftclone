﻿using UnityEngine;
using UnityEngine.UI;

namespace Advertisement
{
    public class ShowAds : MonoBehaviour
    {
        [SerializeField]
        private Button but;
        private void Start()
        {
            but.onClick.AddListener(() => ShowRevardedAds());
        }

        private void ShowRevardedAds()
        {
            IronSource.Agent.showRewardedVideo("DefaultRewardedVideo");
        }
    }
}
