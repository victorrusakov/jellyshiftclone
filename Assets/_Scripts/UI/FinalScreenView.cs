﻿using System.Collections;
using UnityEngine;
using TMPro;

namespace Game.UI.FinalScreen
{
    public class FinalScreenView : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text score;
        [SerializeField]
        private TMP_Text extraScore;

        public void Init(float score, float extraScore)
        {
            StartCoroutine(AnimationScore(score, extraScore));
        }

        IEnumerator AnimationScore(float score, float extraScore)
        {
            bool flag = true;
            float currScore = 0;
            int delta = 10;
            while (currScore <= score)
            {
                currScore += delta;
                this.score.text = Mathf.Clamp(currScore, 0, score).ToString();
                if (this.score.fontSize <= 300 && flag)
                {
                    this.score.fontSize += 5;
                    if (this.score.fontSize >= 300)
                        flag = false;
                }

                if (this.score.fontSize >= 100 && !flag)
                {
                    this.score.fontSize -= 5;
                    if (this.score.fontSize <= 100)
                        flag = true;
                }               
                yield return new WaitForFixedUpdate();
            }
            yield return new WaitForFixedUpdate();

            if(this.score.fontSize <= 300)
            {
                while(this.score.fontSize <= 300)
                {
                    this.score.fontSize += 10;
                    yield return new WaitForFixedUpdate();
                }
            }

            this.extraScore.text = "+ " + extraScore.ToString();

            yield return new WaitForSeconds(3);

            this.extraScore.gameObject.SetActive(false);
            for(int i =0; i<20; i++)
            {
                this.score.rectTransform.position += new Vector3(0, 10, 0);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
