﻿using Game.Purchase;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class RemoveAdsView : MonoBehaviour
    {
        [SerializeField]
        private Button purchase;
        [SerializeField]
        private Button noAds;
        [SerializeField]
        private Purchaser purchaser;
        private void Start()
        {
            purchase.onClick.AddListener(() => Buy());
        }
        void Buy()
        {
            purchaser.RemoveAdsFunc("Remove Ads", noAds);
            gameObject.SetActive(false);
        }
    }
}