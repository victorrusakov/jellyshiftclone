﻿using Game.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.LeaderBoard
{
    public class LeaderBoardController : MonoBehaviour
    {
        [SerializeField]
        private LeaderBoardView boardView;

        [SerializeField]
        private LeaderPeaceView pieceView;

        [SerializeField]
        private Button rename;

        [SerializeField]
        private Button close;

        [SerializeField]
        private RenameView renamePopUp;

        [SerializeField]
        private Transform layout;

        private List<LeaderPeaceView> leadsView;
        void Start()
        {
            rename.onClick.AddListener(() => ReName());
            close.onClick.AddListener(() => Close());
            SetName();
            LeaderBoardInit();
            renamePopUp.OnNameSet += SetName;
        }

        private void Close()
        {
            gameObject.SetActive(false);
        }

        private void ReName()
        {
            renamePopUp.Init();
        }

        private void LeaderBoardInit()
        {
            leadsView = new List<LeaderPeaceView>();
            var data = DataController.Instance().AllPlayerdata.AllPlayers;
            if (data != null)
            {               
                List<string> keyList = new List<string>(data.Keys);                
                List<PlayerData> dataList = new List<PlayerData>();

                for(int i = 0; i < keyList.Count; i++)
                {
                    dataList.Add(data[keyList[i]]);
                }

                var result = from user in dataList
                             orderby user.Score descending
                             select user;
                List<PlayerData> sortedData = new List<PlayerData>();

                foreach (PlayerData u in result)
                {
                    sortedData.Add(u);
                }
                 
                for (int i = 0; i < data.Count; i++)
                {
                    var view = Instantiate(pieceView);
                    view.Init(sortedData[i].Name, sortedData[i].Lvl, sortedData[i].Score);
                    view.transform.SetParent(layout);
                    view.transform.localScale = Vector3.one;
                    leadsView.Add(view);
                }
            }
        }

        public void SetName()
        {
            boardView.SetName(DataController.Instance().Data.Name);
            DataController.Instance().LocalSave();
        }
    }
}
