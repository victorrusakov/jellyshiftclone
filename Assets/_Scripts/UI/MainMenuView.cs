﻿using UnityEngine;
using TMPro;
using Game.Data;

namespace Game.UI.MainMenu
{
    public class MainMenuView : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text lvlText;
        [SerializeField]
        private TMP_Text scoreText;
        [SerializeField]
        private TMP_Text coinsText;
        public void Init(int lvl, float score, int coins)
        {
            lvlText.text = "Lvl - " + lvl.ToString();
            scoreText.text = "score - " + score.ToString();
            coinsText.text = "coins - " + coins.ToString();
            DataController.Instance().OnBuy += SetCoins;
        }

        public void SetCoins()
        {
            coinsText.text = "coins - " + DataController.Instance().Data.Coins.ToString();
        }
    }
}