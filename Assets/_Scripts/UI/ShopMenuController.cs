﻿using Game.Pressets;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Shop
{
    public class ShopMenuController : MonoBehaviour
    {
        [SerializeField]
        private ShopElementView view;
        [SerializeField]
        private Button but;
        [SerializeField]
        private SkyBoxCollection sky;
        [SerializeField]
        private Transform parent;

        private void Start()
        {
            Init();
        }

        public void Init()
        {
            but.onClick.AddListener((Close));
            for (int i = 0; i < sky.Skies.Count; i++)
            {
                var q = Instantiate(view, parent);
                q.Init(i, sky.Skies[i].Cost, sky.Skies[i].Sprites, sky.Skies[i].SkyBoxes);
            }
        }

        public void Open()
        {
            gameObject.SetActive(true);
        }
        private void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
