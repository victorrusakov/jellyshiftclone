﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Game.Data;

namespace Game.UI.LeaderBoard
{
    public class RenameView : MonoBehaviour
    {
        [SerializeField]
        private Button rename;
        [SerializeField]
        private TMP_InputField input;

        private string playerName;

        public void Init()
        {
            gameObject.SetActive(true);
            rename.onClick.AddListener(() => SetName());            
        }
        
        public void SetName()
        {
            playerName = input.text;
            DataController.Instance().Data.Name = playerName;
            gameObject.SetActive(false);
            OnNameSet();
        }

        public delegate void setDelegate();
        public setDelegate OnNameSet;

    }
}
