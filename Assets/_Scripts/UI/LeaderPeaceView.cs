﻿using UnityEngine;
using TMPro;

namespace Game.UI.LeaderBoard
{
    public class LeaderPeaceView : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text nameText;
        [SerializeField]
        private TMP_Text lvlText;
        [SerializeField]
        private TMP_Text scoreText;

        public void Init(string name, int lvl, float score)
        {
            nameText.text = name;
            lvlText.text = lvl.ToString();
            scoreText.text = score.ToString();
        }
    }
}
