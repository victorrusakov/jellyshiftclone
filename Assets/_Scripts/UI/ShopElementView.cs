﻿using Game.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Shop {
    public class ShopElementView : MonoBehaviour
    {
        [SerializeField]
        private Button but;
        [SerializeField]
        private TMP_Text text;

        private int _id;
        private Material skyMat;
        public void Init(int id, int cost, Sprite im, Material mat)
        {
            skyMat = mat;
            _id = id;

            if (DataController.Instance().Data.Skins != null && DataController.Instance().Data.Skins.Contains(id))
            {
                but.onClick.AddListener(() => SetSky());
                text.gameObject.SetActive(false);
                but.image.sprite = im;
            }
            else
            {
                but.onClick.AddListener(() => BuySkin(_id, cost, im));
            }
        }

        private void SetSky()
        {
            RenderSettings.skybox.CopyPropertiesFromMaterial(skyMat);
            DataController.Instance().Data.CurrentSkin = _id;
            DataController.Instance().LocalSave();
        }

        private void BuySkin(int id, int cost, Sprite im)
        {
            but.onClick.RemoveAllListeners();
            if (DataController.Instance().Buy(id, cost))
            {
                text.gameObject.SetActive(false);
                but.image.sprite = im;
                but.onClick.AddListener(() => SetSky());
            }
        }
    }
}
