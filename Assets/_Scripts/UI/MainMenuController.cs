﻿using Game.UI.Shop;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.MainMenu
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField]
        private Button removeAds;
        [SerializeField]
        private Button play;
        [SerializeField]
        private Button leaderBoard;
        [SerializeField]
        private Button shop;
        [SerializeField]
        private MainMenuView menuView;
        [SerializeField]
        private ShopMenuController shopMenu;
        [SerializeField]
        private GameObject leaderBoardWindow;
        [SerializeField]
        private GameObject adsWindow;
        
        private int currLvl;
        private float currScore;
        private int coins;

        public delegate void OnStart();
        public OnStart OnStartGame;
        // Start is called before the first frame update
        void Start()
        {
            menuView.Init(currLvl, currScore, coins);
            shop.onClick.AddListener(()=>shopMenu.Open());
            removeAds.onClick.AddListener(() => RemoveAds());
            play.onClick.AddListener(() => StartPlay());
            leaderBoard.onClick.AddListener(() => ToLeaderBoard());
            if(PlayerPrefs.GetInt("Ads") == 1)
            {
                removeAds.gameObject.SetActive(false);
            }
        }

      
        public void Init(int lvl, float score, int coins)
        {
            currLvl = lvl;
            currScore = score;
            this.coins = coins;
        }
        private void StartPlay()
        {
            OnStartGame();
            gameObject.SetActive(false);
        }

        private void RemoveAds()
        {
            adsWindow.SetActive(true);
        }

        private void ToLeaderBoard()
        {
            
            leaderBoardWindow.SetActive(true);
        }
    }
}