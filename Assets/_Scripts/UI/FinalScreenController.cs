﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.UI.FinalScreen
{
    public class FinalScreenController : MonoBehaviour
    {
        [SerializeField]
        private Button next;
        [SerializeField]
        private FinalScreenView view;
        public void Init(float score, float secondScore)
        {
            gameObject.SetActive(true);
            view.Init(score, secondScore);
            next.onClick.AddListener(() => NextLvl());
        }

        public void NextLvl()
        {          
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            if(PlayerPrefs.GetInt("Ads") != 1)
            {
                IronSource.Agent.showInterstitial();
            }         
        }
    }
}
