﻿using UnityEngine;
using TMPro;

namespace Game.UI.LeaderBoard
{
    public class LeaderBoardView : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text nameText;

        public void SetName(string name)
        {
            nameText.text = name;
        }
    }
}