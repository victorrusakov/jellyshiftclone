﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Pressets
{
    [CreateAssetMenu]
    public class SkyBoxCollection : ScriptableObject
    {
        public List<Sky> Skies;
    }

    [System.Serializable]
    public class Sky
    {
        public Material SkyBoxes;
        public Sprite Sprites;
        public int Cost;
    }
}