﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Pressets
{
    [CreateAssetMenu]
    public class LevelPresset : ScriptableObject
    {
        public List<LevelPiecePresset> Pressets;
        public GameObject Finish;
    }
}

