﻿using Game.LevelObjects.Obstacles;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Pressets
{
    [CreateAssetMenu]
    public class LevelPiecePresset : ScriptableObject
    {
        public List<ObstacleCharacteristics> Pressets;
        public Obstacle Prefab;
    }

    [System.Serializable]
    public class ObstacleCharacteristics
    {
        public List<float> Scales;
        public List<GameObject> Obstacles;
        public int AmountOfObstacles;
    }
}