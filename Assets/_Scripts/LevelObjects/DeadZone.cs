﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.LevelObjects {
    public class DeadZone : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                StartCoroutine(Counter());
            }
        }

        IEnumerator Counter()
        {
            yield return new WaitForSeconds(2);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
