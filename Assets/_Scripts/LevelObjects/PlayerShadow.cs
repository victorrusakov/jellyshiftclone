﻿using UnityEngine;

namespace Game.Player {
    public class PlayerShadow : MonoBehaviour
    {
        [SerializeField]
        private Transform player;
        [SerializeField]
        private bool isHit;
        [SerializeField]
        private GameObject shadow;
        void FixedUpdate()
        {
            int layerMask = 1 << 8;
            layerMask = ~layerMask;
            shadow.transform.localScale = new Vector3(player.transform.localScale.x, player.transform.localScale.y, 1);
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out var hit, Mathf.Infinity, layerMask))
            {
                if (!isHit && hit.collider.gameObject.tag == "Obstacle")
                {
                    isHit = true;
                    shadow.SetActive(true);
                    shadow.transform.position = hit.point - new Vector3(0, 0, 0.2f);
                }
            }
            else
            {
                shadow.SetActive(false);
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.tag == "Shadow" && isHit)
            {
                Debug.Log("0");
                isHit = false;
                shadow.SetActive(false);
            }
        }
    }
}