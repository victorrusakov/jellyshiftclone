﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.LevelObjects.Obstacles
{
    public class ObstacleComponent : MonoBehaviour
    {
        private Rigidbody rg;
        private ParticleSystem ps;
        [SerializeField]
        private BoxCollider bc;
        void Start()
        {
            rg = GetComponent<Rigidbody>();
            ps = GetComponent<ParticleSystem>();
        }


        private void OnCollisionEnter(Collision collision)
        {
            bc.enabled = false;
            StartCoroutine(Destroy());
        }

        IEnumerator Destroy()
        {
            rg.isKinematic = false;            
            ps.Play();
            yield return new WaitForSeconds(0.5f);
            gameObject.SetActive(false);
        }
    }
}
