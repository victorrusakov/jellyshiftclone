﻿using Game.Pressets;
using UnityEngine;

namespace Game.GamePlay.Generator {
    public class LevelGenerator : MonoBehaviour
    {
        [SerializeField]
        private GameObject coin;
        [SerializeField]
        private Transform parent;
        public void Init(LevelPiecePresset[] presset, GameObject finish, int lvlLen)
        {
            float posY = 0;
            for (int i = 0; i != Mathf.Clamp(lvlLen, 0, 20); i++)
            {
                posY = (float)-i / 100;
                var p = presset[Random.Range(0, 9)];
                var q = p.Prefab;
                Instantiate(q, new Vector3(0, posY, i * 10), new Quaternion(), parent);               
                Instantiate(coin, new Vector3(Random.Range(-1f, 1f), posY + 0.8f, i * 10), new Quaternion(), parent);
                q.Init(p, Mathf.Clamp(lvlLen - 1, 0, 9), i * 10, posY, parent);

            }
            Instantiate(finish, new Vector3(0, posY - 0.01f, (Mathf.Clamp(lvlLen, 0, 20)) * 10), new Quaternion());
        }
    }
}
