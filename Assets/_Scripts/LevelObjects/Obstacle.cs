﻿using Game.Pressets;
using UnityEngine;


namespace Game.LevelObjects.Obstacles
{
    public class Obstacle : MonoBehaviour
    {
        
        public void Init(LevelPiecePresset presset, int lvl, int zPos, float yPos, Transform parent)
        {
            zPos -= 3;
            for (int i = 0; i!= presset.Pressets[lvl].AmountOfObstacles; i++)
            {            
                var pr = presset.Pressets[lvl].Obstacles[(Random.Range(0, presset.Pressets[lvl].Obstacles.Count))];
                Instantiate(pr, new Vector3(transform.position.x, transform.position.x + 0.5f+yPos, zPos), transform.rotation, parent.transform);
                var scale = presset.Pressets[lvl].Scales[Random.Range(0, presset.Pressets[lvl].Scales.Count)];
                pr.transform.localScale = new Vector3(Mathf.Clamp(1 - scale, 0.25f, 1.2f), Mathf.Clamp(1 + scale, 0.4f, 1.5f), 1);
                if(scale == 1)
                {
                    pr.transform.localScale = new Vector3(1, 1, 1);
                }
                zPos += (8 / presset.Pressets[lvl].AmountOfObstacles);
            }    
           
        }
    }
}