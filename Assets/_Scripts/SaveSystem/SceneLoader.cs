﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.SceneControl {
	public class SceneLoader : MonoBehaviour
	{
		private AsyncOperation async;

		IEnumerator Start()
		{
			Debug.Log("Loading");
			async = SceneManager.LoadSceneAsync("PlayScene");
			yield return true;
			async.allowSceneActivation = true;
		}
	}
}
