﻿using System.Collections.Generic;

namespace Game.Data
{
    public class PlayerData
    {
        public float Score = 0;
        public string Name = "Nmae";
        public int Lvl = 1;
        public int Coins = 0;
        public List<int> Skins;
        public int CurrentSkin;
        public PlayerData()
        {
        }

        public PlayerData(string name)
        {
            Name = name;
            Score = 0;
            Lvl = 1;
            Coins = 0;
            Skins = new List<int>() {0};
            CurrentSkin = 0;
        }       
    }
}
