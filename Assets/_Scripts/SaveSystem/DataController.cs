﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using Firebase.Database;

namespace Game.Data
{
    public class DataController
    {
        private static DataController instance;
        public static DataController Instance()
        {
            if (instance == null)
            {
                instance = new DataController();
            }
            return instance;
        }

        public PlayerData Data;
        public AllPlayersData AllPlayerdata;
        private DataController()
        {
            AllPlayerdata = new AllPlayersData();
            Load();
        }

        public delegate void onBuy();
        public onBuy OnBuy;

        public bool Buy(int id, int cost)
        {
            if (Data.Coins - cost >= 0)
            {
                Data.Coins -= cost;
                Data.Skins.Add(id);
                Data.CurrentSkin = id;
                OnBuy();
                LocalSave();
                return true;
            }
            else
                return false;
        }

        public void LocalSave()
        {
            string path = Application.persistentDataPath + "/Jelly";
            string json = JsonConvert.SerializeObject(Data);
            File.WriteAllText(path, json);
            FireBaseConnector.UserSavesRef?.SetRawJsonValueAsync(json);
        }

        public void Load()
        {
            string path = Application.persistentDataPath + "/Jelly";
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);
                if (!string.IsNullOrEmpty(json))
                {
                    Data = JsonConvert.DeserializeObject<PlayerData>(json);
                    return;
                }
            }
            Data = new PlayerData("Unnamed");
        }

    }
}
