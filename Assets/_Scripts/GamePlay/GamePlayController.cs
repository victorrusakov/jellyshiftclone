﻿using System.Collections;
using UnityEngine;
using Game.UI.MainMenu;
using Game.Player.Controlling;
using Game.Data;
using Game.LevelObjects.Obstacles;
using Game.UI.FinalScreen;
using Game.Player;
using Game.GamePlay.Generator;
using Game.Pressets;

namespace Game.GamePlay.Controller
{   
    public class GamePlayController : MonoBehaviour
    {
        [SerializeField]
        private MainMenuController mainMenu;
        [SerializeField]
        private PlayerMovement playerMovement;
        [SerializeField]
        private PlayerController playerController;
        [SerializeField]
        private InputHandler inputControls;
        [SerializeField]
        private float playerSpeed = 5;
        [SerializeField]
        private float startSpeed = 5;
        [SerializeField]
        private CameraMove cam;
        [SerializeField]
        private FinalScreenController finalScreen;
        [SerializeField]
        private Obstacle goalObj;
        [SerializeField]
        private Obstacle twinPlatformObj;
        [SerializeField]
        private Obstacle finishObj;
        [SerializeField]
        private LevelGenerator generator;
        [SerializeField]
        private LevelPresset pressets;
        [SerializeField]
        private SkyBoxCollection sky;
        [SerializeField]
        private float baseScore = 0;
        [SerializeField]
        private int obstacleCount = 0;
        [SerializeField]
        private float allScore = 0;
        [SerializeField]
        private int coins = 0;

        private float extraScore;
        private void Awake()
        {
            mainMenu.Init(DataController.Instance().Data.Lvl, DataController.Instance().Data.Score, DataController.Instance().Data.Coins);
        }

        void Start()
        {
            
            RenderSettings.skybox.CopyPropertiesFromMaterial(sky.Skies[DataController.Instance().Data.CurrentSkin].SkyBoxes);
            GenerateLvl();
            mainMenu.OnStartGame += (setPlay);
            mainMenu.OnStartGame += (setSpeed);
            playerController.OnGoalPassed += AddObstacleCount;
            playerController.OnFinishPassed += FinishLvl;
            playerController.OnBlackZoneEnter += ObstacleCountToZero;
            playerController.OnCoinCollected += AddCoins;
            IronSource.Agent.loadBanner(new IronSourceBannerSize(320, 50), IronSourceBannerPosition.BOTTOM);
        }

        private void setSpeed()
        {
            playerSpeed = startSpeed;
            playerMovement.SetSpeed(playerSpeed);
        }

        private void setPlay()
        {
            inputControls.Play = true;
            StartCoroutine(CameraDest());
            cam.SetDumpTime(0);
        }

        private void ObstacleCountToZero()
        {
            obstacleCount = 0;
            playerMovement.SetSpeed(startSpeed);
            playerSpeed = startSpeed;
        }

        private void AddObstacleCount()
        {
            obstacleCount += 1;
            Debug.Log(obstacleCount);
            playerSpeed *= 1.1f;
            playerMovement.SetSpeed(playerSpeed);
        }

        private void ScoreCount()
        {
            playerMovement.SetSpeed(startSpeed);
            cam.SetDumpTime(2);
            cam.SetZPosition(0);
            baseScore = (10 * DataController.Instance().Data.Lvl * (DataController.Instance().Data.Lvl / 10 + 1)/2);
            extraScore = 5 * (obstacleCount / 10) + 5 * coins;
            allScore = baseScore + extraScore;
            DataController.Instance().Data.Score = allScore;
            DataController.Instance().Data.Lvl++;
        }

        private void AddCoins()
        {
            coins += 1;
        }

        private void FinishLvl()
        {
            ScoreCount();
            DataController.Instance().Data.Coins += coins;
            DataController.Instance().LocalSave();
            finalScreen.Init(allScore, extraScore);
        }

        private void GenerateLvl()
        {
            int lvl = DataController.Instance().Data.Lvl;

            var finish = pressets.Finish;
            var presset = pressets.Pressets;
            LevelPiecePresset[] pr = new LevelPiecePresset[10];
            for (int i = 0; i < presset.Count; i++)
            {
                pr[i] = presset[i];
            }
            generator.Init(pr, finish, lvl);
        }

        IEnumerator CameraDest()
        {
            cam.SetZPosition(6);
            int dumpTime = 4;
            for(int i =0; i<4; i++)
            {
                dumpTime -= 1;
                cam.SetDumpTime(dumpTime);
                yield return new WaitForFixedUpdate();
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
