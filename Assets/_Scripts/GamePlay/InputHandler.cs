﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Game.Player.Controlling
{
    public class InputHandler : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        
        

        private bool isDrag;
        private Vector2 position;
        private RectTransform rt;

        public bool Play;

        [System.Serializable]
        public class UnityEventVector2 : UnityEvent<Vector2> { }

        public UnityEventVector2 OnDragChanged;
        private void Start()
        {
            rt = GetComponent<RectTransform>();
        }
    
        public void OnBeginDrag(PointerEventData eventData)
        {
            position = eventData.position;
            isDrag = true;
            
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!isDrag || !Play) return;
            OnDragChanged?.Invoke((eventData.position - position) / rt.sizeDelta);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (!isDrag || !Play) return;
            isDrag = false;
        }
    }
}