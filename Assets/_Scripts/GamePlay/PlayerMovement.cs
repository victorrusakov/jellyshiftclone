﻿using System.Collections;
using UnityEngine;

namespace Game.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        private float moveSpeed = 0;
        private Rigidbody rig;
        void Start()
        {
            rig = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            rig.velocity = new Vector3(0, 0, moveSpeed);
        }

        public void SetSpeed(float Speed)
        {
            moveSpeed = Speed;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "BlackZone")
            {
                rig.AddForce(0, 0, -15, ForceMode.VelocityChange);
                StartCoroutine(SpeedChanger());
            }           
        }

        IEnumerator SpeedChanger()
        {
            if(moveSpeed >= 8)
            {
                moveSpeed -= 7;
                while (moveSpeed < 8)
                {
                    moveSpeed += 2;
                    yield return new WaitForSeconds(0.3f);

                }
            }
            
        }
    }
}