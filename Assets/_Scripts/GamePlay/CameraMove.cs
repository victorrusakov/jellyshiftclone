﻿using UnityEngine;

namespace Game.Player.Controlling
{
    public class CameraMove : MonoBehaviour
    {
        [SerializeField]
        private float dumpTime;
        [SerializeField]
        private Transform target;
        [SerializeField]
        private float zPosDest = 0;

        private Vector3 velocity = Vector3.zero;             
        private Camera cam;
        // Update is called once per frame
        private void Start()
        {
            cam = GetComponent<Camera>();
        }
        void Update()
        {
            if (target)
            {
                Vector3 point = cam.WorldToViewportPoint(new Vector3(target.position.x, target.position.y + 0.75f, target.position.z));
                Vector3 delta = new Vector3(target.position.x, target.position.y + 0.75f, target.position.z) -
                    cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
                Vector3 destination = new Vector3(target.position.x + 2, target.position.y + 3f, target.position.z - zPosDest);
                transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dumpTime);
                transform.LookAt(target);
            }

        }
        public void SetZPosition(int zPos)
        {
            zPosDest = zPos;
        }

        public void SetDumpTime(int time)
        {
            dumpTime = time;
        }
    }
}
