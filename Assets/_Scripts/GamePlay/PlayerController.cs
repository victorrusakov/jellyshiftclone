﻿using UnityEngine;

namespace Game.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] 
        private float speed = 20f;

        private Vector3 destinationScale = Vector3.one;

        public delegate void OnGoal();
        public OnGoal OnGoalPassed;
        public OnGoal OnFinishPassed;
        public OnGoal OnBlackZoneEnter;
        public OnGoal OnCoinCollected;
        void Update()
        {
            transform.localScale = Vector3.Lerp(transform.localScale, destinationScale, Time.deltaTime * speed);
        }

        public void OnInputChange(Vector2 distance)
        {

            float y = Mathf.Clamp(distance.y, -0.8f, 0.8f);
            destinationScale = new Vector3(1 + y, 1 - y, 0.5f);
        }
    
        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == "BlackZone")
            {
                OnBlackZoneEnter();
            }
            if(other.tag == "Obstacle")
            {
                OnGoalPassed();
            }

            if (other.tag == "Finish")
            {
                OnFinishPassed();
                destinationScale = new Vector3(1, 1, 0.5f);
            }

            if (other.tag == "Coin")
            {
                OnCoinCollected();
                other.gameObject.SetActive(false);
            }
        }
    }
}